import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { DetailArticlesComponent } from './detail-articles/detail-articles.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'product-details',
    component: DetailArticlesComponent
  },
  {
    path:'',
    children:[
      {
        path:'',
        component:HomeComponent,
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticlesRoutingModule { }
