import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContentLayoutComponent } from './layout/content-layout/content-layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/articles',
    pathMatch: 'full'
  },
  {
    path: 'articles',
    component: ContentLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./articles/articles.module').then(mod => mod.ArticlesModule)
      }
    ]
  },
  {
    path: 'blog',
    component: ContentLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./blog/blog.module').then(mod => mod.BlogModule)
      }
    ]
  },
  {
    path: 'shop',
    component: ContentLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./shop/shop.module').then(mod => mod.ShopModule)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
